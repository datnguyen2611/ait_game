// src/store/useDeviceStore.ts
import { create } from "zustand";

interface DeviceState {
  isMobile: boolean;
  checkIsMobile: () => void;
}

const useDeviceStore = create<DeviceState>((set) => ({
  isMobile: false,
  checkIsMobile: () => {
    const isMobileDevice =
      /Mobi|Android/i.test(navigator.userAgent) || window.innerWidth <= 768;
    set({ isMobile: isMobileDevice });
  },
}));

export default useDeviceStore;
