import { Carousel } from "antd";

const items: string[] = [
  "banner/banner-small-1.png",
  "banner/banner-small-2.png",
  "banner/banner-small-3.png",
];
const BannerGrid = () => {
  return (
    <Carousel
      dots={false}
      slidesToShow={2.5}
      slidesToScroll={1}
      infinite={false}
      autoplay={false}
      draggable
      className="custom-carousel"
    >
      {items.map((e, key) => {
        return (
          <div
            className="px-2"
            key={key}
          >
            <img
              src={`images/${e}`}
              alt=""
              className="w-full rounded-2xl border-white border-[3px]"
            />
          </div>
        );
      })}
    </Carousel>
  );
};
export default BannerGrid;
