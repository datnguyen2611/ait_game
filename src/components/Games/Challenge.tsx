import LockIcon from "@/assets/css/icons/LockIcon";
import StarIcon from "@/assets/css/icons/StarIcon";
import { cn } from "@/lib/utils";
import { Link } from "react-router-dom";

type itemProps = {
  img?: string;
  title?: string;
  request: number;
  highScore?: number;
  href: string;
};
const items: itemProps[] = [
  {
    img: "https://cdn.gp.mana.vn/photos/9/1/5/915606d13ec10274e21defa0081ad1f6.jpg",
    title: "Bubble Shooter",
    request: 2000,
    highScore: 10000,
    href: "https://game.itsaverse.com/bubblepop/",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/d/5/3/d53b959e290b675599c254b75ef43a50.jpg",
    title: "Table tennis",
    request: 2000,
    highScore: 10000,
    href: "https://tabletennis.h5games.usercontent.goog/v/24f1dad4-8ec6-4a10-8700-168324e23c86/",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/9/1/5/915606d13ec10274e21defa0081ad1f6.jpg",
    title: "Pastry Paradise",
    request: 2000,
    highScore: 10000,
    href: "https://cdn.ludigames.com/h5/pastryParadiseFree/play.html",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/8/9/1/891f4fe8-342e-4479-823b-28060c4df5cc-origin.jpeg",
    title: "Siberian Strike",
    request: 2000,
    highScore: 10000,
    href: "https://cdn.ludigames.com/h5/siberianStrikeFree/play.html",
  },
];

const Challenge = () => {
  return (
    <>
      <div className="text-lg font-bold">Challenge</div>
      <div className="grid grid-cols-1 sm:grid-cols-2 gap-4">
        {items.map((e, key) => {
          return (
            <div
              className={cn(
                "w-full flex flex-row flex-shrink-0 pr-2 overflow-hidden rounded-xl shadow-md ",
                key === 0
                  ? "bg-orange-300 dark:bg-orange-400"
                  : "bg-white dark:bg-gray-800",
              )}
              key={key}
            >
              <div
                className="w-2/5 flex-shrink-0 bg-cover"
                style={{ backgroundImage: `url(${e.img})` }}
              />
              <div className="flex flex-col gap-1 w-full py-2 pl-4 pr-0 text-xs text-black dark:text-white">
                <Link
                  to={e.href}
                  className="text-black dark:text-white"
                >
                  {e.title}
                </Link>
                <div>Request: Get {e.request} Points</div>
                <div>High Score: {e.highScore}</div>
                <div>Reward:</div>
                <div className="flex flex-row justify-between items-center">
                  <div className="flex flex-row gap-1">
                    <div className="flex rounded-sm bg-orange-500 text-white w-4 h-4 items-center justify-center">
                      <StarIcon />
                    </div>
                    <div className="flex rounded-sm bg-orange-500 text-white w-4 h-4 items-center justify-center">
                      <StarIcon />
                    </div>
                    <div className="flex rounded-sm bg-orange-500 text-white w-4 h-4 items-center justify-center">
                      <StarIcon />
                    </div>
                    <div className="flex rounded-sm bg-green-500 text-white w-4 h-4 items-center justify-center">
                      <LockIcon />
                    </div>
                    <div className="flex rounded-sm bg-green-500 text-white w-4 h-4 items-center justify-center">
                      <LockIcon />
                    </div>
                  </div>
                  <div className="flex flex-row items-center">
                    <span
                      className={cn(
                        "text-xs py-[2px] px-4 mr-[-14px] ",
                        key === 0
                          ? "text-white bg-orange-500 rounded-full"
                          : "text-blue-500 dark:text-blue-300",
                      )}
                    >
                      5/100
                    </span>
                    <img
                      src="/images/button/diamond.png"
                      className="w-7 h-7"
                    />
                  </div>
                </div>
                <Link
                  to={e.href}
                  className="text-black dark:text-white self-end"
                >
                  <button className="p-2 text-white font-bold rounded-full px-6 shadow-md bg-blue-600">
                    PLAY
                  </button>
                </Link>
              </div>
            </div>
          );
        })}
      </div>
    </>
  );
};
export default Challenge;
