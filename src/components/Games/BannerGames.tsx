import { Carousel } from "antd";

const items: string[] = [
  "banner/banner-1.jpg",
  "banner/banner-2.jpg",
  "banner/banner-3.jpg",
];
const BannerGames = () => {
  return (
    <Carousel
      className="uk-slider-container"
      dots={false}
      draggable
    >
      {items.map((e, key) => {
        return (
          <div
            className="uk-slider-items"
            key={key}
          >
            <img
              src={`images/${e}`}
              alt=""
              className="w-full rounded-2xl"
            />
          </div>
        );
      })}
    </Carousel>
  );
};
export default BannerGames;
