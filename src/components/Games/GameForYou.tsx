import { Link } from "react-router-dom";

type itemProps = {
  img: string;
  title: string;
  href: string;
  type: string;
};

const items: itemProps[] = [
  {
    img: "https://cdn.gp.mana.vn/photos/4/5/7/4574a89f-3f2b-4bd2-b1c1-335cfe95643c-origin.jpeg",
    href: "https://cdn.ludigames.com/h5/pastryParadiseFree/play.html",
    title: "Paradise",
    type: "Sport",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/5/2/f/52f0379d673716f0a4edf6f61bc5c75c.jpg",
    href: "https://hikigame.com/cocos-game/HyperSnake/",
    title: "HyperSnake",
    type: "Popular",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/d/5/3/d53b959e290b675599c254b75ef43a50.jpg",
    href: "https://tabletennis.h5games.usercontent.goog/v/24f1dad4-8ec6-4a10-8700-168324e23c86/",
    title: "Table tennis",
    type: "Sport",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/a/3/c/a3c25b4f88a1f7f49616b60f213fd963.jpg",
    href: "https://game.itsaverse.com/otter-story/",
    title: "Otter story",
    type: "Popular",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/4/5/7/4574a89f-3f2b-4bd2-b1c1-335cfe95643c-origin.jpeg",
    href: "https://cdn.ludigames.com/h5/pastryParadiseFree/play.html",
    title: "Paradise",
    type: "Sport",
  },
  {
    img: "https://cdn.gp.mana.vn/photos/d/5/3/d53b959e290b675599c254b75ef43a50.jpg",
    href: "https://tabletennis.h5games.usercontent.goog/v/24f1dad4-8ec6-4a10-8700-168324e23c86/",
    title: "Table tennis",
    type: "Sport",
  },
];
const GameForYou = () => {
  return (
    <div className="bg-white dark:bg-gray-800 rounded-xl p-3 pr-0 flex flex-col gap-2 shadow-sm">
      <div className="text-lg font-bold">For you</div>
      <div className="hide-scrollbar overflow-x-scroll">
        <div className="flex flex-row gap-2">
          {items.map((e, key) => {
            return (
              <div
                className="w-[108px] flex flex-col gap-2 flex-shrink-0 pr-2 overflow-x-hidden"
                key={key}
              >
                <Link
                  to={e.href}
                  className="text-black dark:text-white"
                >
                  <img
                    src={e.img}
                    className="rounded-md sm:aspect-square h-20 w-20"
                  />
                  <h4 className="font-semibold text-sm uppercase text-nowrap overflow-ellipsis">
                    {e.title}
                  </h4>
                  <span className="text-sm">{e.type}</span>
                  <button
                    type="button"
                    className="button border"
                  >
                    Play now
                  </button>
                </Link>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};
export default GameForYou;
