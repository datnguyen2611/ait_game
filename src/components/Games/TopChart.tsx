import { Profile2User } from "iconsax-react";
import { Link } from "react-router-dom";

type itemProps = {
  img: string;
  title: string;
  href: string;
  type: string;
  player: number;
};
const items: itemProps[] = [
  {
    img: "https://cdn.gp.mana.vn/photos/a/3/c/a3c25b4f88a1f7f49616b60f213fd963.jpg",
    href: "https://game.itsaverse.com/otter-story/",
    title: "Otter story",
    type: "Popular",
    player: 1500,
  },
  {
    img: "https://cdn.gp.mana.vn/photos/9/1/5/915606d13ec10274e21defa0081ad1f6.jpg",
    href: "https://game.itsaverse.com/bubblepop/",
    title: "Bubble Shooter",
    type: "Popular",
    player: 1500,
  },
];

const TopChart = () => {
  return (
    <div className="g-white dark:bg-gray-800 rounded-xl p-3 flex flex-col gap-2 shadow-sm">
      <div className="text-lg font-bold">Top Chart</div>
      <div className="flex flex-row overflow-hidden gap-2">
        {items.map((e, key) => {
          return (
            <div
              className="w-1/2 flex flex-row gap-2 flex-shrink-0 pr-3"
              key={key}
            >
              <Link
                to={e.href}
                className="flex-shrink-0 text-black dark:text-white"
              >
                <img
                  src={e.img}
                  className="rounded-md sm:aspect-square h-20 w-20 object-cover"
                  alt=""
                />
              </Link>
              <div className="flex flex-col gap-1">
                <Link
                  to={""}
                  className="w-20 sm:w-52 text-sm font-bold uppercase whitespace-nowrap overflow-hidden overflow-ellipsis"
                >
                  {e.title}
                </Link>
                <div className="text-xs">
                  <span className="text-xs">{e.type}</span>
                </div>
                <div className="text-sm flex flex-row items-center text-blue-500 dark:text-blue-400">
                  <Profile2User size={14} />
                  {e.player} Player
                </div>
              </div>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default TopChart;
