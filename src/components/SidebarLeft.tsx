import { cn } from "@/lib/utils";
import {
  ArrowCircleDown,
  ArrowCircleUp,
  Card,
  Logout,
  Setting2,
} from "iconsax-react";
import { useState } from "react";
import Scrollbars from "react-custom-scrollbars-2";
import { Link, useLocation } from "react-router-dom";

type NavItemProps = {
  icon: string;
  path: string;
  title: string;
  hidden: boolean;
};

const SidebarLeft = () => {
  const [seeMore, setSeeMore] = useState(false);
  const location = useLocation();
  const navItems: NavItemProps[] = [
    {
      icon: "icons/game.png",
      path: "/games",
      title: "games",
      hidden: false,
    },
    {
      icon: "icons/home.png",
      path: "/feed",
      title: "Feed",
      hidden: false,
    },
    {
      icon: "icons/message.png",
      path: "/messages",
      title: "messages",
      hidden: false,
    },
    {
      icon: "icons/video.png",
      path: "/video",
      title: "video",
      hidden: false,
    },
    {
      icon: "icons/event.png",
      path: "/event",
      title: "event",
      hidden: false,
    },
    {
      icon: "icons/page.png",
      path: "/pages",
      title: "Pages",
      hidden: false,
    },
    {
      icon: "icons/group.png",
      path: "/groups",
      title: "Groups",
      hidden: false,
    },
    {
      icon: "icons/market.png",
      path: "/market",
      title: "market",
      hidden: false,
    },
    {
      icon: "icons/blog.png",
      path: "/blog",
      title: "blog",
      hidden: false,
    },

    {
      icon: "icons/event-2.png",
      path: "/event2",
      title: "Event II",
      hidden: true,
    },
    {
      icon: "icons/group-2.png",
      path: "/groups2",
      title: "Groups II",
      hidden: true,
    },
  ];
  const sortedNavItems = navItems.sort((a, b) => {
    if (a.hidden && !b.hidden) return 1;
    if (!a.hidden && b.hidden) return -1;
    return 0;
  });

  const handleClickShow = () => {
    setSeeMore(!seeMore);
  };

  return (
    <Scrollbars
      autoHide
      className="p-2 max-xl:bg-white shadow-sm 2xl:w-72 sm:w-64 w-[80%] h-[calc(100vh-64px)] relative z-30 max-lg:border-r dark:max-xl:!bg-slate-700 dark:border-slate-700"
    >
      <div>
        <div className="p-2">
          <nav id="side">
            <ul>
              {sortedNavItems.map((e, key) => {
                return (
                  <li
                    className={cn(
                      location.pathname === e.path ? "active" : "",
                      e.hidden && !seeMore && "hidden",
                    )}
                    key={key}
                  >
                    <Link
                      to={e.path}
                      className="text-black dark:text-white"
                    >
                      <img
                        src={`/images/${e.icon}`}
                        alt={e.title}
                        className="w-6"
                      />
                      <span> {e.title} </span>
                    </Link>
                  </li>
                );
              })}
            </ul>
            <div
              className="z-20  backdrop-blur-sm flex align-middle text-black dark:text-white gap-4 p-4 cursor-pointer"
              onClick={handleClickShow}
            >
              {seeMore ? (
                <ArrowCircleUp
                  size={20}
                  variant="Bold"
                />
              ) : (
                <ArrowCircleDown
                  size={20}
                  variant="Bold"
                />
              )}
              <span className="">See {seeMore ? "Less" : "More"}</span>
            </div>
          </nav>
          <nav
            id="side"
            className="font-medium text-sm text-black border-t pt-3 mt-2 dark:text-white dark:border-slate-800"
          >
            <div className="px-3 pb-2 text-sm font-medium">
              <div className="text-black dark:text-white">Pages</div>
            </div>
            <ul
              className="mt-2 -space-y-2"
              uk-nav="multiple: true"
            >
              <li>
                <Link
                  to={"/setting"}
                  className="text-black dark:text-white"
                >
                  <Setting2 size={24} />
                  <span> Setting </span>
                </Link>
              </li>
              <li>
                <Link
                  to={"/update"}
                  className="text-black dark:text-white"
                >
                  <Card size={24} />
                  <span> Upgrade </span>
                </Link>
              </li>
              <li>
                <Link
                  to={"/login"}
                  className="text-black dark:text-white"
                >
                  <Logout size={24} />
                  <span> Authentication </span>
                </Link>
              </li>

              <li>
                <Link
                  to={"/deverloper"}
                  className="text-black dark:text-white"
                >
                  <Card size={24} />
                  <span> Deverloper </span>
                </Link>
              </li>
            </ul>
          </nav>

          <div className="text-xs font-medium flex flex-wrap gap-2 gap-y-0.5 p-2 mt-2">
            <a
              href="games.html#"
              className="hover:underline"
            >
              About
            </a>
            <a
              href="games.html#"
              className="hover:underline"
            >
              Blog{" "}
            </a>
            <a
              href="games.html#"
              className="hover:underline"
            >
              Careers
            </a>
            <a
              href="games.html#"
              className="hover:underline"
            >
              Support
            </a>
            <a
              href="games.html#"
              className="hover:underline"
            >
              Contact Us{" "}
            </a>
            <a
              href="games.html#"
              className="hover:underline"
            >
              Developer
            </a>
          </div>
        </div>
      </div>
    </Scrollbars>
  );
};
export default SidebarLeft;
