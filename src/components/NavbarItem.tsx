import { Icon } from "iconsax-react";
import { Link, useLocation } from "react-router-dom";
import { cn } from "../lib/utils";

interface SidebarItemProps {
  label: string;
  href: string;
  icon: Icon;
}

const NavbarItem: React.FC<SidebarItemProps> = ({
  href,
  icon: Icon,
  label,
}) => {
  const location = useLocation();
  const isActive = href === location.pathname;

  return (
    <Link
      to={href}
      className={cn(
        "flex w-full flex-col items-center gap-[2px] text-center",
        isActive ? "text-primary hover:bg-main" : "text-darkGray ",
      )}
    >
      <Icon variant={isActive ? "Bold" : "Linear"} />
      <p className="font-normal text-xs">{label}</p>
    </Link>
  );
};

export default NavbarItem;
