import { cn } from "@/lib/utils";
import {
  DiscountShape,
  DocumentText,
  Game,
  Home3,
  Shop,
  Wallet,
} from "iconsax-react";
import { Pages } from "../types";
import NavbarItem from "./NavbarItem";

const NavbarMobile = () => {
  const routes = [
    {
      path: Pages.home,
      title: "Apps",
      icon: Home3,
    },
    {
      path: Pages.games,
      title: "Games",
      icon: Game,
    },
    {
      path: Pages.feed,
      title: "Feed",
      icon: DocumentText,
    },
    {
      path: Pages.marketplace,
      title: "Marketplace",
      icon: Shop,
    },
    {
      path: Pages.discount,
      title: "Ưu đãi",
      icon: DiscountShape,
    },
    {
      path: Pages.wallet,
      title: "Wallet",
      icon: Wallet,
    },
  ];

  return (
    <div
      className={cn(
        "z-[100] h-[--m-top] fixed bottom-0 left-0 w-full flex items-center bg-white/80 sky-50 backdrop-blur-xl border-b border-slate-200 dark:bg-dark2 dark:border-slate-800",
      )}
    >
      {routes.map((route, key) => (
        <div
          key={key}
          className="w-full"
        >
          <NavbarItem
            href={route.path}
            label={route.title}
            icon={route.icon}
          />
        </div>
      ))}
    </div>
  );
};
export default NavbarMobile;
