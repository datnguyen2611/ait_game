import { Link } from "react-router-dom";

type ItemProps = {
  href: string;
  img: string;
  title: string;
  player: string;
};
const SitebarTopGames = () => {
  const items: ItemProps[] = [
    {
      title: "Minecraft",
      img: "games/img-3.jpg",
      href: "games.html#",
      player: "258 are playing",
    },
    {
      title: " Mobile Legends",
      img: "games/img-4.jpg",
      href: "games.html#",
      player: "612 friends are playing",
    },
    {
      title: "Larion TV5 ",
      img: "games/img-3.jpg",
      href: "games.html#",
      player: "428 friends are playing",
    },
    {
      title: "Fortninte",
      img: "games/img-6.jpg",
      href: "games.html#",
      player: "258 friends are playing",
    },
    {
      title: " Battle Grounds",
      img: "games/img-1.jpg",
      href: "games.html#",
      player: " 612 friends are playing",
    },
  ];
  return (
    <div className="box p-5 px-6">
      <div className="flex items-baseline justify-between text-black dark:text-white">
        <h3 className="font-bold text-base"> Top Games </h3>
        <a
          href="games.html#"
          className="text-sm text-blue-500"
        >
          See all
        </a>
      </div>

      <div className="side-list">
        {items.map((e, key) => {
          return (
            <div
              className="side-list-item"
              key={key}
            >
              <Link to={e.href}>
                <img
                  src={`/images/${e.img}`}
                  alt=""
                  className="side-list-image rounded-md"
                />
              </Link>
              <div className="flex-1">
                <a href="games.html#">
                  <h4 className="side-list-title">{e.title} </h4>
                </a>
                <div className="side-list-info"> {e.player} </div>
              </div>
              <button className="button border">Play</button>
            </div>
          );
        })}

        <button className=" button w-full mt-2 ">See all</button>
      </div>
    </div>
  );
};
export default SitebarTopGames;
