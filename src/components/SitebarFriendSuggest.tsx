import { Refresh } from "iconsax-react";
import { Link } from "react-router-dom";
type ItemProps = {
  avatar: string;
  name: string;
  href: string;
};

const SiteBarFriendSuggest = () => {
  const items: ItemProps[] = [
    {
      avatar: "avatars/avatar-7.jpg",
      href: "timeline",
      name: "Johnson smith",
    },
    {
      avatar: "avatars/avatar-5.jpg",
      href: "timeline",
      name: " James Lewis",
    },
    {
      avatar: "avatars/avatar-2.jpg",
      href: "timeline",
      name: "John Michael",
    },
    {
      avatar: "avatars/avatar-3.jpg",
      href: "timeline",
      name: "Monroe Parker",
    },
    {
      avatar: "avatars/avatar-4.jpg",
      href: "timeline",
      name: " Martin Gray",
    },
  ];
  return (
    <div className="box p-5 px-6 border1 dark:bg-dark2">
      <div className="flex justify-between text-black dark:text-white">
        <h3 className="font-bold text-base"> Peaple You might know </h3>
        <button type="button">
          {" "}
          <Refresh />
        </button>
      </div>
      <div className="space-y-4 capitalize text-xs font-normal mt-5 mb-2 text-gray-500 dark:text-white/80">
        {items.map((e, key) => {
          return (
            <div
              className="flex items-center gap-3"
              key={key}
            >
              <Link to={e.href}>
                <img
                  src={`/images/${e.avatar}`}
                  alt=""
                  className="bg-gray-200 rounded-full w-10 h-10"
                />
              </Link>
              <div className="flex-1">
                <a href="timeline.html">
                  <h4 className="font-semibold text-sm text-black dark:text-white">
                    {e.name}
                  </h4>
                </a>
                <div className="mt-0.5"> Suggested For You </div>
              </div>
              <button
                type="button"
                className="text-sm rounded-full py-1.5 px-4 font-semibold bg-secondery"
              >
                {" "}
                Follow{" "}
              </button>
            </div>
          );
        })}
      </div>
    </div>
  );
};
export default SiteBarFriendSuggest;
