import CardIcon from "@/assets/css/icons/CardIcon";
import FlashIcon from "@/assets/css/icons/FlashIcon";
import LogoutIcon from "@/assets/css/icons/LogoutIcon";
import LoundSpeakerIcon from "@/assets/css/icons/LoundSpeakerIcon";
import MoonIcon from "@/assets/css/icons/MoonIcon";
import SettingIcon from "@/assets/css/icons/SettingIcon";
import { cn } from "@/lib/utils";
import { Popover, Switch } from "antd";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const Profile = () => {
  const [open, setOpen] = useState(false);
  const [darkMode, setDarkMode] = useState<boolean>(() => {
    // Check localStorage for user preference
    const savedTheme = localStorage.getItem("theme");
    if (savedTheme) {
      return savedTheme === "dark";
    }
    // Check system preference
    return (
      window.matchMedia &&
      window.matchMedia("(prefers-color-scheme: dark)").matches
    );
  });

  useEffect(() => {
    const root = document.documentElement;
    if (darkMode) {
      root.classList.add("dark");
      localStorage.setItem("theme", "dark");
    } else {
      root.classList.remove("dark");
      localStorage.setItem("theme", "light");
    }
  }, [darkMode]);

  const toggleTheme = (checked: boolean) => {
    setDarkMode(checked);
  };

  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };
  const profile = {
    avatar: "avatars/avatar-2.jpg",
    userName: "Stell johnson",
    hasTag: " mohnson",
  };

  const title = (
    <div className="border-b border-solid dark:border-gray-600/60">
      <Link to={"/profile"}>
        <div className="p-4 py-5 flex items-center gap-4">
          <img
            src={`/images/${profile.avatar}`}
            alt=""
            className="w-10 h-10 rounded-full shadow"
          />
          <div className="flex-1">
            <h4 className="text-sm font-medium text-black dark:text-white">
              {profile.userName}
            </h4>
            <div className="text-sm mt-1 text-blue-600 font-light dark:text-white/70">
              @{profile.hasTag}
            </div>
          </div>
        </div>
      </Link>
    </div>
  );

  const content = (
    <div className="p-2 text-sm text-black font-normal dark:text-white">
      <Link
        to={"/"}
        className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-blue-600"
      >
        <FlashIcon />
        Upgrade To Premium
      </Link>
      <Link
        to={"/setting"}
        className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-black dark:text-white"
      >
        <CardIcon />
        My Billing
      </Link>
      <Link
        to={"/setting"}
        className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-black dark:text-white"
      >
        <LoundSpeakerIcon />
        Advatacing
      </Link>
      <Link
        to={"/setting"}
        className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-black dark:text-white"
      >
        <SettingIcon />
        My Account
      </Link>
      <div className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-black dark:text-white">
        <MoonIcon />
        Night mode
        <Switch
          checked={darkMode}
          onChange={toggleTheme}
          className={cn(
            "bg-slate-200/40 ml-auto p-0.5 rounded-full w-9 dark:hover:bg-white/20  ",
            darkMode ? "switch-dark" : "switch-light",
          )}
        />
      </div>
      <div className="border-t border-solid dark:border-gray-600/60">
        <Link
          to={"/login"}
          className="flex items-center gap-2.5 hover:bg-secondery p-2 px-2.5 rounded-md dark:hover:bg-white/10 text-black dark:text-white"
        >
          <LogoutIcon />
          Log Out
        </Link>
      </div>
    </div>
  );
  return (
    <Popover
      content={content}
      title={title}
      arrow={false}
      trigger="hover"
      open={open}
      onOpenChange={handleOpenChange}
      placement="bottomRight"
      overlayClassName={cn(
        "custom-popover",
        "bg-white rounded-lg drop-shadow-xl dark:bg-slate-700 w-64 border2",
        darkMode ? "dark" : "light",
      )}
      overlayInnerStyle={{ padding: "0", backgroundColor: "unset" }}
    >
      <div className="rounded-full relative bg-secondery cursor-pointer shrink-0">
        <img
          src={`/images/${profile.avatar}`}
          alt=""
          className="sm:w-9 sm:h-9 w-7 h-7 rounded-full shadow shrink-0"
        />
      </div>
    </Popover>
  );
};
export default Profile;
