import { cn } from "@/lib/utils";
import { Input, Popover } from "antd";
import { Edit, LayoutMaximize, Messages3, SearchNormal1 } from "iconsax-react";
import { useState } from "react";
import { Link } from "react-router-dom";

type PropsItems = {
  avatar?: string;
  name?: string;
  time?: string;
  message?: string;
};

const MessagerHeader = () => {
  const [open, setOpen] = useState(false);
  const darkMode = localStorage.getItem("theme");
  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
  };

  const items: PropsItems[] = [
    {
      avatar: "avatars/avatar-2.jpg",
      name: "Jesse Steeve",
      time: "09:40AM",
      message: "Love your photos 😍",
    },
    {
      avatar: "avatars/avatar-4.jpg",
      name: " Martin Gray",
      time: "02:40AM",
      message: " Product photographer wanted? 📷",
    },
    {
      avatar: "avatars/avatar-5.jpg",
      name: "maika",
      time: "2 day",
      message: "Want to buy some weed ? 🌄",
    },
    {
      avatar: "avatars/avatar-3.jpg",
      name: "Monroe Parker",
      time: "4 week",
      message: "I’m glad you like it.😊",
    },
    {
      avatar: "avatars/avatar-7.jpg",
      name: "Alex Dolve",
      time: "2 month",
      message: "Photo editor needed. Fix photos? 🛠️",
    },
    {
      avatar: "avatars/avatar-4.jpg",
      name: "Jenny",
      time: " 09:40AM",
      message: " Love your photos 😍",
    },
  ];

  const title = (
    <>
      <div className="flex items-center justify-between gap-2 p-4 pb-1">
        <h3 className="font-bold text-xl"> Chats </h3>

        <div className="flex gap-2.5 text-lg text-slate-900 dark:text-white">
          <LayoutMaximize size={18} />
          <Edit size={18} />
        </div>
      </div>

      <div className="relative w-full p-2 px-3 ">
        <Input
          type="text"
          className="w-full !pl-10 !rounded-lg dark:!bg-white/10"
          placeholder="Search"
          prefix={
            <SearchNormal1
              size={16}
              className="dark:text-white absolute left-7 -translate-y-1/2 top-1/2"
            />
          }
        />
      </div>
    </>
  );

  const content = (
    <div className="">
      <div className="h-80 overflow-y-auto pr-2">
        <div className="p-2 pt-0 pr-1 dark:text-white/80">
          {items.map((e, key) => {
            return (
              <Link
                to={"/messager"}
                key={key}
                className="relative flex items-center gap-4 p-2 py-3 duration-200 rounded-lg hover:bg-secondery dark:hover:bg-white/10 text-black dark:text-white"
              >
                <div className="relative w-10 h-10 shrink-0">
                  <img
                    src={`images/${e.avatar}`}
                    alt=""
                    className="object-cover w-full h-full rounded-full"
                  />
                </div>
                <div className="flex-1 min-w-0">
                  <div className="flex items-center gap-2 mb-1">
                    <div className="mr-auto text-sm text-black dark:text-white font-medium">
                      {e.name}
                    </div>
                    <div className="text-xs text-gray-500 dark:text-white/80">
                      {e.time}
                    </div>
                    <div className="w-2.5 h-2.5 bg-blue-600 rounded-full dark:bg-slate-700"></div>
                  </div>
                  <div className="font-normal overflow-hidden text-ellipsis text-xs whitespace-nowrap">
                    {e.message}
                  </div>
                </div>
              </Link>
            );
          })}
        </div>
      </div>
      <Link to={"/message"}>
        <div className="text-center py-4 border-t border-slate-100 text-sm font-medium text-blue-600 dark:text-white dark:border-gray-600">
          See all Messages{" "}
        </div>
      </Link>
    </div>
  );

  return (
    <Popover
      content={content}
      title={title}
      trigger="click"
      open={open}
      onOpenChange={handleOpenChange}
      placement="bottomRight"
      overlayClassName={cn(
        "custom-popover bg-white rounded-lg   dark:bg-slate-700 md:w-[360px] ",
        darkMode,
      )}
      overlayInnerStyle={{
        background: "none",
      }}
    >
      <button className="sm:p-2 p-1 rounded-full relative sm:bg-secondery dark:text-white">
        <Messages3
          size={24}
          className="text-dark2 dark:text-white"
        />
      </button>
    </Popover>
  );
};
export default MessagerHeader;
