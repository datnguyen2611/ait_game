import { cn } from "@/lib/utils";
import { Badge, Popover } from "antd";
import { More, Notification, Setting2, Slash, TickCircle } from "iconsax-react";
import { useState } from "react";
import { Link } from "react-router-dom";

type PropsItems = {
  avatar?: string;
  name?: string;
  time?: string;
  message?: string;
  isRead: boolean;
};

const NotificationHeader = () => {
  const [open, setOpen] = useState(false);
  const [openMore, setOpenMore] = useState(false);
  const darkMode = localStorage.getItem("theme");
  const handleOpenChange = (newOpen: boolean) => {
    setOpen(newOpen);
    setOpenMore(false);
  };
  const handleOpenMoreChange = (open: boolean) => {
    setOpenMore(open);
  };

  const items: PropsItems[] = [
    {
      avatar: "avatars/avatar-3.jpg",
      name: "Alexa Gray",
      time: "4 hours ago",
      message: " started following you. Welcome him to your profile. 👋",
      isRead: false,
    },
    {
      avatar: "avatars/avatar-7.jpg",
      name: "Jesse Steeve",
      time: " 8 hours ago",
      message: "mentioned you in a story. Check it out and reply. 📣",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-6.jpg",
      name: "Alexa stella",
      time: " 8 hours ago",
      message: "ommented on your photo “Wow, stunning shot!” 💬",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-2.jpg",
      name: "John Michael",
      time: "2 hours ago",
      message: "who you might know, is on socialite.💬",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-3.jpg",
      name: " Sarah Gray",
      time: "4 hours ago",
      message: "sent you a message. He wants to chat with you. 💖",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-4.jpg",
      name: "Jesse Steev",
      time: "8 hours ago",
      message: "sarah tagged you <br> in a photo of your birthday party. 📸",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-2.jpg",
      name: "Lewis Lewis",
      time: "8 hours ago",
      message: "mentioned you in a story. Check it out and reply. 📣",
      isRead: true,
    },
    {
      avatar: "avatars/avatar-7.jpg",
      name: "Martin Gray",
      time: "8 hours ago ",
      message: "liked your photo of the Eiffel Tower. 😍",
      isRead: true,
    },
  ];

  const contentMoreOption = (
    <div className="w-[280px] group">
      <div className="text-sm">
        <Link
          to={"#"}
          className="flex items-center gap-3 rounded-md py-2 px-3"
        >
          <TickCircle
            className="text-xl shrink-0"
            size={20}
          />
          Mark all as read
        </Link>
        <Link
          to={"#"}
          className="flex items-center gap-3 rounded-md py-2 px-3"
        >
          <Setting2
            className="text-xl shrink-0"
            size={20}
          />
          Notification setting
        </Link>
        <Link
          to={"#"}
          className="flex items-center gap-3 rounded-md py-2 px-3"
        >
          <Slash
            className="text-xl shrink-0"
            size={20}
          />
          Mute Notification
        </Link>
      </div>
    </div>
  );
  const title = (
    <div className="flex items-center justify-between gap-2 p-4 pb-2">
      <h3 className="font-bold text-xl"> Notifications </h3>
      <div className="flex gap-2.5">
        <Popover
          content={contentMoreOption}
          placement="bottomRight"
          arrow={false}
          trigger="click"
          open={openMore}
          onOpenChange={handleOpenMoreChange}
        >
          <More
            size={20}
            className="p-1 flex rounded-full focus:bg-secondery dark:text-white"
          />
        </Popover>
      </div>
    </div>
  );

  const content = (
    <>
      <div className="text-sm h-[400px] w-full overflow-y-auto pr-2">
        <div className="pl-2 p-1 text-sm font-normal dark:text-white">
          {items.map((e, key) => {
            return (
              <Link
                to={"/notification"}
                key={key}
                className={cn(
                  "relative flex items-center gap-3 p-2 duration-200 rounded-xl pr-10 hover:bg-secondery dark:hover:bg-white/10 text-black dark:text-white",
                  !e.isRead && "bg-teal-500/5",
                )}
              >
                <div className="relative w-12 h-12 shrink-0">
                  {" "}
                  <img
                    src={`/images/${e.avatar}`}
                    alt=""
                    className="object-cover w-full h-full rounded-full"
                  />
                </div>
                <div className="flex-1">
                  <p>
                    {" "}
                    <b className="font-bold mr-1"> {e.name}</b> {e.message}
                  </p>
                  <div className="text-xs text-gray-500 mt-1.5 dark:text-white/80">
                    {" "}
                    {e.time}{" "}
                  </div>
                  {!e.isRead && (
                    <div className="w-2.5 h-2.5 bg-teal-600 rounded-full absolute right-3 top-5"></div>
                  )}
                </div>
              </Link>
            );
          })}
        </div>
      </div>
      <Link to={"/notification"}>
        <div className="text-center py-4 border-t border-slate-100 text-sm font-medium text-blue-600 dark:text-white dark:border-gray-600">
          View Notifications{" "}
        </div>
      </Link>
    </>
  );

  return (
    <Popover
      content={content}
      title={title}
      trigger="click"
      open={open}
      onOpenChange={handleOpenChange}
      placement="bottomRight"
      overlayClassName={cn(
        "custom-popover bg-white rounded-lg   dark:bg-slate-700 md:w-[360px] ",
        darkMode,
      )}
      overlayInnerStyle={{
        background: "none",
      }}
    >
      <Badge
        count={5}
        offset={[-6, 6]}
        styles={{
          indicator: {
            height: "16px",
            lineHeight: "16px",
            minWidth: "16px",
            boxShadow: "none",
          },
        }}
      >
        <button className="sm:p-2 p-1 rounded-full relative sm:bg-secondery dark:text-white">
          <Notification
            size={24}
            className="text-dark2 dark:text-white"
          />
        </button>
      </Badge>
    </Popover>
  );
};
export default NotificationHeader;
