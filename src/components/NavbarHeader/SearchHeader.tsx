import { cn } from "@/lib/utils";
import { AutoComplete, Input } from "antd";
import { CloseCircle, SearchNormal1 } from "iconsax-react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

type Props = {
  value: string;
  setValue: (value: string) => void;
};
type Options = {
  avatar: string;
  name: string;
  type: string;
};

const SearchHeader: React.FC<Props> = () => {
  const [searchValue, setSearchValue] = useState("");
  const [options, setOptions] = useState<Options[]>([]);
  const darkMode = localStorage.getItem("theme");
  const items: Options[] = [
    {
      avatar: "avatars/avatar-2.jpg",
      name: "Jesse Steeve",
      type: "Friend",
    },
    {
      avatar: "avatars/avatar-2.jpg",
      name: "Martin Gray",
      type: "Friend",
    },
    {
      avatar: "group/group-2.jpg",
      name: "Delicious Foods",
      type: "Group",
    },
    {
      avatar: "group/group-1.jpg",
      name: "Delicious Foods 4",
      type: "Page",
    },
  ];
  useEffect(() => {
    setOptions(items);
  }, []);

  // Update recent searches in local storage and state
  const updateRecentSearches = (query: string) => {
    const updatedSearches = options.filter((item) => item.name !== query);

    setOptions(updatedSearches); // Keep only the 5 most recent searches
  };

  // Handle change in search input value
  const handleSearchChange = (value: string) => {
    setSearchValue(value);
    // Update recent searches
    updateRecentSearches(value);
  };

  // Handle click on autocomplete input

  // Handle click on clear button
  const handleClearClick = () => {
    // Clear recent searches
    setOptions([]);
    console.log(options);
  };

  const renderOption = (option: Options) => {
    return (
      <Link
        to={"/"}
        className="relative px-3 py-1.5 flex items-center gap-4 hover:bg-secondary  dark:hover:bg-white/10"
      >
        <img
          src={`/images/${option.avatar}`}
          className="w-9 h-9 rounded-full"
          alt="avatar"
        />
        <div>
          <div className="text-black dark:text-white">{option.name}</div>
          <div className="text-xs text-blue-500 font-medium mt-0.5">
            {option.type}
          </div>
        </div>
        <CloseCircle
          className="text-base absolute right-3 top-1/2 -translate-y-1/2 dark:text-white"
          size={16}
        />
      </Link>
    );
  };
  return (
    <AutoComplete
      options={items.map((item) => ({
        value: item.name,
        label: renderOption(item),
      }))}
      className="xl:w-[694px] sm:w-96 p-0 max-md:hidden bg-secondery focus-within:border-white  w-screen left-0 max-sm:fixed max-sm:top-2 dark:!bg-white/5"
      onSelect={(value) => setSearchValue(value)}
      dropdownStyle={{ padding: "0" }}
      dropdownRender={(menu) => (
        <div className="bg-white dark:bg-dark3 w-full  rounded-lg  p-0">
          <div className="flex justify-between px-2 py-2.5 text-sm font-medium ">
            <p className=" text-black dark:text-white">Recent</p>
            <button
              type="button"
              className="text-blue-500"
              onClick={handleClearClick}
            >
              Clear
            </button>
          </div>
          {menu}
        </div>
      )}
    >
      <Input
        placeholder="Search Friends, videos .."
        value={searchValue}
        onChange={(e) => handleSearchChange(e.target.value)}
        className={cn(
          " w-full !pl-10 !font-normal !bg-transparent h-10 !text-sm text-black dark:text-white",
          `placeholder-${darkMode}`,
        )}
        prefix={
          <SearchNormal1
            className="mr-1"
            size={20}
          />
        }
        suffix={null} // Remove the search icon on the right
      />
    </AutoComplete>
  );
};
export default SearchHeader;
