import { CloseCircle, HambergerMenu } from "iconsax-react";
import { useState } from "react";
import { Link } from "react-router-dom";
import MessagerHeader from "./Message";
import NotificationHeader from "./Notification";
import Profile from "./Profile";
import SearchHeader from "./SearchHeader";

type Props = {
  collapse: boolean;
  setCollapse: (e: boolean) => void;
};
const NavbarHeader = ({ collapse, setCollapse }: Props) => {
  const [valueSearch, setValueSearch] = useState<string>("");
  const handleCollapse = () => {
    setCollapse(!collapse);
  };

  return (
    <header className="z-[100] h-[--m-top] fixed top-0 left-0 w-full flex items-center bg-white/80 sky-50 backdrop-blur-xl border-b border-slate-200 dark:bg-dark2 dark:border-slate-800">
      <div className="flex items-center justify-between w-full xl:px-6 px-2 max-lg:gap-10 relative">
        <div className=" 2xl:w-[--w-side] lg:w-[--w-side-sm]">
          <div className="flex items-center gap-1">
            <button
              className="flex items-center justify-center w-8 h-8 text-xl rounded-full xl:hidden  group"
              onClick={handleCollapse}
            >
              {collapse ? (
                <CloseCircle className=" text-2xl " />
              ) : (
                <HambergerMenu className="text-2xl " />
              )}
            </button>
            <div
              id="logo"
              className="w-10 h-10"
            >
              <Link to="/">
                <img
                  src="/images/logo.png"
                  alt=""
                  className="max-w-full block"
                />
              </Link>
            </div>
          </div>
        </div>
        <div className="flex-1 relative">
          <div className="max-w-[1220px] mx-auto flex items-center">
            <SearchHeader
              value={valueSearch}
              setValue={setValueSearch}
            />
          </div>
          <div className="flex items-center sm:gap-4 gap-2 absolute right-5 top-1/2 -translate-y-1/2 text-black">
            <MessagerHeader />
            <NotificationHeader />
            <Profile />
          </div>
        </div>
      </div>
    </header>
  );
};
export default NavbarHeader;
