import NavbarHeader from "@/components/NavbarHeader";
import NavbarMobile from "@/components/NavbarMobile";
import SidebarLeft from "@/components/SidebarLeft";
import { cn } from "@/lib/utils";
import useDeviceStore from "@/store/useDeviceStore";
import { useState } from "react";
import { Outlet } from "react-router-dom";

const MainLayout = () => {
  const { isMobile } = useDeviceStore();
  const [collapse, setCollapse] = useState(false);
  const domain = window.location.origin;
  console.log(domain);

  return (
    <div
      id="wrapper"
      className="h-screen w-full relative "
    >
      <NavbarHeader
        collapse={collapse}
        setCollapse={setCollapse}
      />

      <div
        id="site__sidebar"
        className={cn(
          "fixed top-0 left-0 z-[99] pt-[--m-top] overflow-hidden transition-transform xl:duration-500 max-xl:w-full  ",
          collapse ? "!-translate-x-0" : "max-xl:-translate-x-full",
        )}
      >
        <SidebarLeft />
      </div>

      <main
        id="site__main"
        className={cn(
          "2xl:ml-[--w-side]  xl:ml-[--w-side-sm] p-2.5 h-[calc(100vh-var(--m-top))] mt-[--m-top]",
          // isMobile && "mb-[--m-top]",
        )}
      >
        <Outlet />
      </main>

      {isMobile && domain !== "http://34.126.162.29:8082/" && <NavbarMobile />}
    </div>
  );
};

export default MainLayout;
