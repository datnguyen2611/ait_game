export interface RouteItem {
  label: string;
  href: string;
  icon?: React.ReactNode;
  children?: RouteItem[];
}
export enum Pages {
  games = "/games",
  feed = "/feed",
  marketplace = "/marketplace",
  discount = "/discount",
  home = "/",
  wallet = "/wallet",
}
