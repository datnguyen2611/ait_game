import { StyleProvider } from "@ant-design/cssinjs";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import { Toaster } from "sonner";
import App from "./App.tsx";
import "./index.css";
import MainLayout from "./layouts/MainLayout.tsx";
import Feed from "./pages/Feed.tsx";
import Games from "./pages/Games.tsx";
import { Pages } from "./types/index.ts";
import Home from "./pages/Home.tsx";
import Marketplace from "./pages/Marketplace.tsx";
import Discount from "./pages/Discount.tsx";
import Wallet from "./pages/Wallet.tsx";

const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      {
        element: <MainLayout />,
        children: [
          {
            path: Pages.home,
            element: <Home />,
            index: true,
          },
          {
            path: Pages.games,
            element: <Games />,
          },
          {
            path: Pages.feed,
            element: <Feed />,
          },
          {
            path: Pages.marketplace,
            element: <Marketplace />,
          },
          {
            path: Pages.discount,
            element: <Discount />,
          },
          {
            path: Pages.wallet,
            element: <Wallet />,
          },
        ],
      },
    ],
  },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <div className="relative h-full w-full main-wraper">
    <StyleProvider hashPriority="high">
      <RouterProvider router={router} />
      <App />
      <Toaster
        richColors
        position="top-center"
        toastOptions={{ style: { fontFamily: "'AvertaStdCY', sans-serif" } }}
      />
    </StyleProvider>
  </div>,
);
