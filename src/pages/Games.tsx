import AllGame from "@/components/Games/AllGame";
import BannerGames from "@/components/Games/BannerGames";
import BannerGrid from "@/components/Games/BannerGrid";
import Challenge from "@/components/Games/Challenge";
import GameForYou from "@/components/Games/GameForYou";
import TopChart from "@/components/Games/TopChart";
import SiteBarFriendSuggest from "@/components/SitebarFriendSuggest";
import SitebarTopGames from "@/components/SitebarTopGames";
import { cn } from "@/lib/utils";
import { Button } from "antd";
import { useState } from "react";
import { Link } from "react-router-dom";

type BoxItemProps = {
  img: string;
  title: string;
  href: string;
};
const boxItems: BoxItemProps[] = [
  {
    title: "Coin/point free",
    img: "icons/coin-point-free.png",
    href: "/",
  },
  {
    title: "Spin",
    img: "icons/spin.png",
    href: "/",
  },
  {
    title: "Lucky Box",
    img: "icons/lucky-box.png",
    href: "/",
  },
  {
    title: "Daily quest",
    img: "icons/daily-quest.png",
    href: "/",
  },
  {
    title: "Tutorial",
    img: "icons/tutorial.png",
    href: "/",
  },
  {
    title: "Online Reward",
    img: "icons/online-reward.png",
    href: "/",
  },
  {
    title: "Monthly Pass",
    img: "icons/monthly-pass.png",
    href: "/",
  },
  {
    title: "PVP",
    img: "icons/pvp.png",
    href: "/",
  },
  {
    title: "Tournament",
    img: "icons/tournament.png",
    href: "/",
  },
  {
    title: "Social",
    img: "icons/social.png",
    href: "/",
  },
];

const Games = () => {
  const [switcherButton, setSwitcherButton] = useState("character");
  return (
    <div
      className="flex max-lg:flex-col 2xl:gap-12 gap-10 2xl:max-w-[1220px] max-w-[1065px] mx-auto"
      id="js-oversized"
    >
      <div className="flex-1">
        <div className="max-w-[680px]  w-full mx-auto content-area flex flex-col items-center relative ">
          <div
            className="z-10 w-auto mx-auto absolute rounded-full inline-flex flex-row justify-center bg-gray-400 bg-opacity-50 text-white m-4 mt-4 p-2 gap-2"
            uk-switcher="connect: #tabs; animation: uk-animation-fade"
          >
            <Button
              className={cn(
                "button-switcher p-2 rounded-full px-4 text-black bg-white",
                switcherButton === "character" && "uk-active",
              )}
              onClick={() => {
                setSwitcherButton("character");
              }}
            >
              Character
            </Button>
            <Button
              className={cn(
                "button-switcher p-2 rounded-full px-4 text-black bg-white",
                switcherButton === "dailyQuest" && "uk-active",
              )}
              onClick={() => {
                setSwitcherButton("dailyQuest");
              }}
            >
              Daily Quest
            </Button>
            <Button
              className={cn(
                "button-switcher p-2 rounded-full px-4 text-black bg-white",
                switcherButton === "achievements" && "uk-active",
              )}
              onClick={() => {
                setSwitcherButton("achievements");
              }}
            >
              Achievements
            </Button>
          </div>
          <div className="max-w-[680px] w-full absolute flex flex-col items-end">
            <div className="flex flex-col gap-2 mt-20 sm:mt-28 mr-6">
              <img
                src="/images/button/avatar.png"
                className="w-16 h-16 drop-shadow-lg rounded-full"
              />
              <img
                src="/images/button/pets.png"
                className="w-16 h-16 drop-shadow-lg rounded-full"
              />
              <img
                src="/images/button/decor.png"
                className="w-16 h-16 drop-shadow-lg rounded-full"
              />
              <img
                src="/images/button/bg.png"
                className="w-16 h-16 drop-shadow-lg rounded-full"
              />
              <img
                src="/images/button/skin.png"
                className="w-16 h-16 drop-shadow-lg rounded-full"
              />
            </div>
          </div>
          <img
            src="/images/gameImages.png"
            alt=""
            className="w-full"
          />
        </div>
        <div className="max-w-[680px] w-full mx-auto content-area p-4 sm:p-6">
          <div className="grid md:grid-cols-5 grid-cols-5 md:gap-8 gap-4">
            {boxItems.map((e, key) => {
              return (
                <div
                  className="uk-scrollspy-inview "
                  key={key}
                >
                  <Link
                    className="flex flex-col items-center text-black dark:text-white"
                    to={e.href}
                  >
                    <div className="aspect-[1/1] w-full">
                      <img
                        src={`/images/${e.img}`}
                        alt=""
                        className="w-full"
                      />
                    </div>
                    {/* <div className="max-w-[680px] w-full mx-auto content-area p-4 sm:p-6"> */}
                    <h4 className="text-sm text-center">{e.title}</h4>
                    <p className="card-text"></p>
                    {/* </div> */}
                  </Link>
                </div>
              );
            })}
          </div>
        </div>
        <div className="max-w-[680px] mx-auto p-4 uk-slider">
          <BannerGames />
        </div>
        <div className="max-w-[680px] mx-auto pl-4">
          <BannerGrid />
        </div>
        <div className="max-w-[680px] mx-auto p-4 pb-0">
          <GameForYou />
        </div>
        <div className="max-w-[680px] mx-auto p-4 pb-0">
          <TopChart />
        </div>
        <div className="max-w-[680px] mx-auto p-4 pb-0">
          <AllGame />
        </div>
        <div className="max-w-[680px] mx-auto p-4 pb-0 space-y-2">
          <Challenge />
        </div>
        <div className="max-w-[680px] mx-auto p-4 mb-16 md:mb-0">
          <img
            className="w-full rounded-xl"
            src="images/banner/tournament.png"
          />
        </div>
      </div>
      <div className="2xl:w-[380px] lg:w-[330px] w-full hidden sm:block">
        <div className="lg:space-y-6 space-y-4 lg:pb-8 max-lg:grid sm:grid-cols-2 max-lg:gap-6">
          <SitebarTopGames />
        </div>
        <SiteBarFriendSuggest />
      </div>
    </div>
  );
};
export default Games;
