/** @type {import('tailwindcss').Config} */
export default {
  content: ["./index.html", "./src/**/*.{js,ts,jsx,tsx}"],
  darkMode: "class",
  important: true,
  theme: {
    extend: {
      colors: {
        primary: "rgb(var(--color-primary) / <alpha-value>)",
        secondary: "rgb(var(--color-bg-secondary) / <alpha-value>)",
        main: "rgb(var(--color-bg-primary) / <alpha-value>)",
        gray1: "rgb(var(--color-border) / <alpha-value>)",
        white1: "rgba(255, 255, 255, 0.75)",
        darkGray: "#888F97",
        dark2: " rgb(var(--color-dark2) / <alpha-value>)",
        dark: "#1A1C20",
        gray: {
          25: "#FCFCFD",
          50: "#F9FAFB",
          100: "#F2F4F7",
          200: "#EAECF0",
          300: "#D0D5DD",
          400: "#98A2B3",
          500: "#667085",
          600: "#475467",
          700: "#344054",
          800: "#1D2939",
          900: "#101828",
          950: "#0C111D",
        },
        warning: {
          25: "#FFFCF5",
          50: "#FFFAEB",
          100: "#FEF0C7",
          200: "#FEDF89",
          300: "#FEC84B",
          400: "#FDB022",
          500: "#F79009",
          600: "#DC6803",
          700: "#B54708",
          800: "#93370D",
          900: "#7A2E0E",
          950: "#4E1D09",
        },
        neutral: {
          50: "rgba(242, 242, 242, 1)",
        },
      },
      backgroundColor: {
        dark3: "rgb(var(--bg-dark3) / <alpha-value>)",
      },

      boxShadow: {
        4: "0px 4px 12px 0px rgba(0, 0, 0, 0.08)",
        0: " 0px 0px 28px 0px rgba(0, 0, 0, 0.08)",
      },
      dropShadow: {
        gray: " 0px -0.33px 0px 0px rgba(0, 0, 0, 0.3)",
      },
      keyframes: {
        swing: {
          "0%,100%": { transform: "rotate(15deg)" },
          "50%": { transform: "rotate(-15deg)" },
        },
      },
      animation: {
        swing: "swing 1s infinite",
      },
      borderColor: "red",
    },
  },
  plugins: [],
};
